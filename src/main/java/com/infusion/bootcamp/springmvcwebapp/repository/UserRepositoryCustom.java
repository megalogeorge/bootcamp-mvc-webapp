package com.infusion.bootcamp.springmvcwebapp.repository;

import com.infusion.bootcamp.springmvcwebapp.model.User;

public interface UserRepositoryCustom {
    User upsert(User user);
}
