package com.infusion.bootcamp.springmvcwebapp.repository;

import com.infusion.bootcamp.springmvcwebapp.model.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

@Repository
public class UserRepositoryImpl implements UserRepositoryCustom {
    @Autowired private MongoDbFactory mongoDbFactory;
    public User upsert(User user) {
        MongoTemplate mongoTemplate = new MongoTemplate(mongoDbFactory);
        Query query = new Query(Criteria.where("_id").is(user.getId()));
        User _user = mongoTemplate.findOne(query, User.class);
        if (_user != null) {
            _user.setFirstName(user.getFirstName());
            _user.setLastName(user.getLastName());
            mongoTemplate.save(_user);
            return _user;
        } else {
            mongoTemplate.save(user);
            return user;
        }
    }
}
