package com.infusion.bootcamp.springmvcwebapp.repository;

import com.infusion.bootcamp.springmvcwebapp.model.User;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface UserRepository extends PagingAndSortingRepository<User, Long>, UserRepositoryCustom {
    List<User> findByFirstName(String firstName);

    @Query("{$or:[{firstName: ?0},{lastName: ?0}]}")
    List<User> findByAnyName(String name);
}
