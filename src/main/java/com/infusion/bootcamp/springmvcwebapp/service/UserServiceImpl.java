package com.infusion.bootcamp.springmvcwebapp.service;

import com.infusion.bootcamp.springmvcwebapp.repository.UserRepository;
import com.infusion.bootcamp.springmvcwebapp.model.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    @Autowired private UserRepository userRepository;

    public List<User> findUsersByAnyName(String name) {
        List<User> usersByAnyName = userRepository.findByAnyName(name);

        if (usersByAnyName.isEmpty()) {
            throw new IllegalArgumentException("No users exist that have the name " + name);
        }
        return usersByAnyName;
    }

    public List<User> findUsersByFirstName(final String firstName) {
        List<User> usersByFirstName = userRepository.findByFirstName(firstName);

        if (usersByFirstName.isEmpty()) {
            throw new IllegalArgumentException("No users exist with the first name " + firstName);
        }
        return usersByFirstName;
    }

    public User save(User user) {
        return userRepository.upsert(user);
    }
}
