package com.infusion.bootcamp.springmvcwebapp.service;

import com.infusion.bootcamp.springmvcwebapp.model.User;

import java.util.List;

public interface UserService {
    List<User> findUsersByAnyName(String name);

    List<User> findUsersByFirstName(String firstName);

    User save(User user);
}
