package com.infusion.bootcamp.springmvcwebapp.controller;

import com.infusion.bootcamp.springmvcwebapp.model.User;
import com.infusion.bootcamp.springmvcwebapp.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {
    @Autowired private UserService userService;

    @RequestMapping(value = "/find/{firstName}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<User> findUsersByFirstName(@PathVariable String firstName) {
        return userService.findUsersByFirstName(firstName);
    }

    @RequestMapping(value = "/findAny/{name}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<User> findUsersByAnyName(@PathVariable String name) {
        return userService.findUsersByAnyName(name);
    }

    @RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public User save(@RequestBody User user) {
        return userService.save(user);
    }
}
