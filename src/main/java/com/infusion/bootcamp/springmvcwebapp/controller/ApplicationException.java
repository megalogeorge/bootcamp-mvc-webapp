package com.infusion.bootcamp.springmvcwebapp.controller;

import com.google.common.base.MoreObjects;

import java.util.Objects;

public class ApplicationException {
    private String code;
    private String message;

    public ApplicationException(String code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        ApplicationException that = (ApplicationException) o;
        return Objects.equals(code, that.code);
    }

    public String getCode() {

        return code;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public int hashCode() {
        return Objects.hash(code);
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this).add("code", code).add("message", message).toString();
    }
}
