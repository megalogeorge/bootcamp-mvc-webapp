package com.infusion.bootcamp.springmvcwebapp.controller;

import com.google.common.collect.Lists;
import com.infusion.bootcamp.springmvcwebapp.model.User;
import com.infusion.bootcamp.springmvcwebapp.service.UserService;
import com.infusion.bootcamp.springmvcwebapp.service.UserServiceImpl;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.List;

public class UserControllerTest {
    @InjectMocks private UserController userController;
    @Mock private UserService userService;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        List<User> users = Lists.newArrayList(new User("George", "Pai"));
        Mockito.when(userService.findUsersByFirstName("George")).thenReturn(users);
        Mockito.when(userService.findUsersByFirstName("Bob")).thenThrow(new IllegalArgumentException("No users exist with the first name Bob"));
    }

    @Test
    public void testFindUsersByFirstNameHappyPath() {
        List<User> usersByFirstName = userController.findUsersByFirstName("George");
        Assert.assertNotNull(usersByFirstName);
        Assert.assertEquals(1, usersByFirstName.size());
    }

    @Test
    public void testFindUsersByFirstNameExceptionalPath() {
        try {
            userController.findUsersByFirstName("Bob");
            Assert.fail("Should have thrown IllegalArgumentException");
        } catch (IllegalArgumentException e) {
            Assert.assertEquals("No users exist with the first name Bob", e.getMessage());
        }
    }

}
