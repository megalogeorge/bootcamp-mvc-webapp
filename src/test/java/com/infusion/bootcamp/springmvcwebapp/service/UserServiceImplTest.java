package com.infusion.bootcamp.springmvcwebapp.service;

import com.google.common.collect.Lists;
import com.infusion.bootcamp.springmvcwebapp.model.User;
import com.infusion.bootcamp.springmvcwebapp.repository.UserRepository;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

public class UserServiceImplTest {
    @Mock private UserRepository userRepository;
    @InjectMocks private UserServiceImpl userService;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        List<User> users = Lists.newArrayList(new User("George", "Pai"));
        Mockito.when(userRepository.findByFirstName("George")).thenReturn(users);
        Mockito.when(userRepository.findByFirstName("Bob")).thenReturn(new ArrayList<User>());
    }

    @Test
    public void testFindUsersByFirstNameHappyPath() {
        List<User> usersByFirstName = userService.findUsersByFirstName("George");
        Assert.assertNotNull(usersByFirstName);
        Assert.assertEquals(1, usersByFirstName.size());
    }

    @Test
    public void testFindUsersByFirstNameExceptionalPath() {
        try {
            userService.findUsersByFirstName("Bob");
            Assert.fail("Should have thrown IllegalArgumentException");
        } catch (IllegalArgumentException e) {
            Assert.assertEquals("No users exist with the first name Bob", e.getMessage());
        }
    }
}
